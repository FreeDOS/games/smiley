# Smiley, version 2021-11-10 alpha

A simple Pong-style game (VGA, 386+) mostly for testing various aspects of the Danger Engine. Requires VGA and 386+. A Mouse is highly recommended.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

# SMILEY.LSM

<table>
<tr><td>title</td><td>Smiley</td></tr>
<tr><td>version</td><td>2021-11-10 alpha</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-11-10</td></tr>
<tr><td>description</td><td>A simple Pong-style game</td></tr>
<tr><td>summary</td><td>A simple Pong-style game (VGA, 386+) mostly for testing various aspects of the Danger Engine. Requires VGA and 386+. A Mouse is highly recommended.</td></tr>
<tr><td>keywords</td><td>dos game</td></tr>
<tr><td>author</td><td>Jerome Shidel <jerome _AT_ shidel.net></td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel <jerome _AT_ shidel.net></td></tr>
<tr><td>primary&nbsp;site</td><td>[https://fd.lod.bz/repos/current/pkg-html/smiley.html](https://fd.lod.bz/repos/current/pkg-html/smiley.html)</td></tr>
<tr><td>alternate&nbsp;site</td><td>[https://gitlab.com/DangerApps/smiley](https://gitlab.com/DangerApps/smiley)</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[BSD 3-Clause License](LICENSE)</td></tr>
</table>
